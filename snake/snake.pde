
IntList x = new IntList();
IntList y = new IntList();
int xEat = 4;
int yEat = 5;
boolean eatYN = true;
boolean gameOver = false;

void setup()
{
  size(600, 600); 
  x.append(0);
  y.append(0);  
};
//===========================================================================================

final int widthSnake = 100; //ширина змейки
final int heightSnake = 100; // высота змейки              
final int speedSnake = 700; //скрость змейки



final int up = 1;
final int right = 2;
final int down = 3;
final int left = 4;

int turn = down; 
/*
      1 up

4 left     2 right
  
      3 down
*/
              
//============================================================================================
void draw()
{
  
  final int M = width/widthSnake; // кол-во ячеек для змейки по ширине
  final int N = height/heightSnake; // кол-во ячеек для змейки по высоте
 
  background(0);
  
  //-----обнуляет мейку из за проигрыша
  if (gameOver == true)
  {
    x.clear();
    y.clear();
    x.append(0);
    y.append(0);
    gameOver = false; 
  }
  
  //-----рисует сетки
  stroke(0, 200,0);
  for (int i=0; i<M; i++)
    {line(i*widthSnake, 0, i*widthSnake, height);}
    
  for (int i=0; i<N; i++)
    {line(0, i*heightSnake, width, i*heightSnake);}
  
  //задает место положение еды змейки
  if (eatYN)
  {
    xEat = int(random(0, M));
    yEat = int(random(0, N)); 
    
    eatYN = false;
  }
  
  
  //---рисует змейку //<>//
  for (int i=0; i<x.size(); i++)
    {rect(x.get(i)*widthSnake, y.get(i)*heightSnake, heightSnake, widthSnake);}
  rect(xEat*widthSnake, yEat*heightSnake, heightSnake, widthSnake);

  
  //-----проверяет села змейка еду илли нет
  if (x.get(0) == xEat && y.get(0) == yEat)
  {
    x.append(x.get(y.size()-1)+1);
    y.append(y.get(y.size()-1)+1);
    eatYN = true;
  }
  
  //---тянет хвост за мордой
  for (int i=x.size()-1; i > 0; i--)
  {
    x.set(i, x.get(i-1));
    y.set(i, y.get(i-1));
  }
  
  
  //-----изменяет координаты морды змеи 
  if (turn == up) //<>//
    {y.set(0, y.get(0)-1);}
  else if (turn == right)
         {x.set(0, x.get(0)+1);}
  else if (turn == down)
         {y.set(0, y.get(0)+1);}
  else if (turn == left)
         {x.set(0, x.get(0)-1);}
   
  

  
  //---проверяет не вышла ли змейка за границы экрана
  if (x.get(0) >= M)
    {x.set(0,0);}
  else if (x.get(0) < 0)
         {x.set(0,M);}
  else if (y.get(0) >= N)
         {y.set(0,0);}
  else if (y.get(0) < 0)
         {y.set(0,N);}
    
  
  //проверяет врезалась ли змейка в себя
  for (int i=2; i<x.size(); i++)
  {
    if (x.get(0) == x.get(i) && y.get(0) == y.get(i))
    {
     gameOver = true;
     break;
    }
  }
  delay(speedSnake);
};
//============================================================================================ //<>//
void keyPressed()
{
  //-----изменяет движение змейки
  if (turn == left || turn == right)
  {
    if (key == 'w')
      {turn = up;}
   
    else if (key == 's')
           {turn = down;}
  }
  else  
  {
    if (key == 'd')
         {turn = right;}
 
    else if (key == 'a')
         {turn = left;}
  
  } 
};
//============================================================================================