/*
 Программа рисует так называемое фрактальное дерево. 
 Рисует ствол с определенной толщиной, цветом, высотой и координатами.
 Рисует от конца этого ствола линии под определенным углом, длины, цветом и шириной, и
 от каждого конца линии рисует еще такие же линии, но с определенно меньшей длиной 
 и с изменением опреденного угла
 и начиная с определенного момента поменятся цвет линии.
*/

//================================================================================================================================================
float radianLeft = PI/4;//Поворорот линии в радианах на лево от pi/2
float radianRight = PI/6;//Поворорот линии в радианах на право от pi/2
//------------------
final float longColorGreen = 15; // Начиная с какой длины линии начнут рисаовать зеленым цветом
final float longExit = 9; // Длина линии определяющая прекращение рисования линии
//------------------
final float changeWidthLine = 0.7; //Изменяет толщину линии в k раз
final float changeLongLine = 0.8; //Изменяет длину линии в k раз
//------------------
float beginWidthLine = 15;//Начальная ширина линии
float beginLongLine = 100; //Начальная длина линии 
//------------------
final color stickColor = #8F6F20;//Цвет "типо" ветки 
final color foliageColor = #349C34;//Цвет "типо" листьев 
//------------------начальные координаты ствола
final float x1Trunk = 600; 
final float y1Trunk = 600;
final float x2Trunk = 600;
final float y2Trunk = 600-beginLongLine;
//------------------текст
final float heightWork = round(height/20);
final float heightBetweenString = 10;
final float begingXText = 40;
final float beginYText = 40;
//------------------scrollBar
final float rangeValueLongLine = 200;
float x=100;
float widthTreeY=80;
float w=15;
float h=15;
boolean yN = false;
//------------------счетчик для подсчета кол-во веток
int count =0;
//================================================================================================================================================






/*Функция рисует дерево и принимает следующие аргументы (float x координаты точки к котрой надо нарисовать линию и от которой все линии пойдут дальше
                                                         float y координаты точки к котрой надо нарисовать линию и от которой все линии пойдут дальше
                                                         float l начальная длина ветки
                                                         flaot PI/2+ radL наклон левой ветки в радианах относительно вертекали
                                                         flaot weightLine ширина ветки)
*/

void tree(float x,float y, float l, float rad, float weightLine)
{ 
//------------------
  //подсчитывает кол-во веток
  count++;  
//------------------
  //Вычисления координат для отрисовки линии
  float x1 = x - l*cos(rad);
  float y1 = y - l*sin(rad);
  float x2 = x - l*cos(rad-radianLeft-radianRight);
  float y2 = y - l*sin(rad-radianLeft-radianRight);
//------------------  
//Задания цвета дерева и ширины
  if (l>longColorGreen)
    {stroke(stickColor);}//Начальный цвет дерева в RGB (ветки)
  else {stroke(foliageColor);}//Конечный цвет дерева в RGB (листья)

  strokeWeight(weightLine);
//------------------
  //Отрисовка линий
  line (x, y, x1, y1);
  line (x, y, x2, y2);
//------------------  
  //Рекурсия для отрисовки всех линий
  if (l>longExit)
  {
    tree( x1,  y1,  l*changeLongLine,  rad+radianLeft,   weightLine*changeWidthLine);
    tree( x2,  y2,  l*changeLongLine,  rad-radianRight,  weightLine*changeWidthLine);
  }
//------------------

};

//================================================================================================================================================
void setup()
{
//------------------
 fullScreen();
 //size(1200,700); 
//------------------
};


//================================================================================================================================================

void draw()
{  
//------------------
  //создает джвижение дерева
  background(0); //<>//
  radianLeft= (PI)/width * float(mouseX);
  radianRight= (PI)/height * float(mouseY);
//------------------
  //Отрисовка ствола дерева 
  strokeWeight(beginWidthLine);
  stroke(stickColor);//Цвет ствола дерева
  line (x1Trunk, y1Trunk, x2Trunk, y2Trunk); //<>// //<>//
//------------------
  //Рисует дерево
  tree(x2Trunk, y2Trunk,  beginLongLine*changeLongLine,  PI/2+radianLeft,  beginWidthLine);
//------------------
 printOnDysplay(); 
//------------------ 
  //обнуляем счетчик веток
  count=0;
//------------------отрисовка scrollBar
rect(x, widthTreeY, w, h);
};
//================================================================================================================================================
void mousePressed()
{
  if ((mouseX > x) && (mouseX < x+w) && (mouseY > widthTreeY) && (mouseY < widthTreeY+h))
  {
  yN = true;
  }
  
};
//================================================================================================================================================
void mouseDragged()
{
 
  if (yN)
  {
    x=mouseX;
    beginLongLine=mouseX/(width/rangeValueLongLine);
  }
};
//================================================================================================================================================
void mouseClicked()
{
yN= false;
};
//================================================================================================================================================
void keyPressed()
{
//------------------изменяет пропорционально высоту дерева 's'вниз   'w'вверх
 if ((beginLongLine > 1) && (key == 's'))
   {beginLongLine-=1;}
 else if (key == 'w')
        {beginLongLine+=1;}
//------------------изменяет пропорционально ширину дерева 'd' шире    'a' уже
if ((beginWidthLine > 1) && (key == 'a'))
   {beginWidthLine-=1;}
 else if (key == 'd')
        {beginWidthLine+=1;}

};
//================================================================================================================================================
void printOnDysplay()
{
  float xText =begingXText;
  float yText =beginYText;
  String str;
 //пишет в градусах на сколько повернута левая ветка 
  str = "left " + str(degrees((PI)/width * float(mouseX)));
  text(str, xText, yText);
  yText += heightWork+heightBetweenString;
//------------------
   //пишет в градусах на сколько повернута левая ветка 
  str = "right " + str(degrees((PI)/height * float(mouseY)));
  text(str, xText, yText);
  yText += heightWork+heightBetweenString;
//------------------
  //выводит на экран высоу начальной палки
  str = "long tree " + str(beginLongLine);
  text(str, xText,yText);
  yText += heightWork+heightBetweenString;
//------------------
  //выводит на экран ширину палки
  str = "width tree " + str(beginWidthLine);
  text(str, xText, yText);
  yText += heightWork+heightBetweenString;
//------------------
  //выводит кол-во веток на экран
  str = "Count " + str(count);
  text(str, xText,yText);
};